import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class SimpleWalker extends Walker  {

	private  Color walkerColor ;
	private  Color ShadowColor ;
	
	public SimpleWalker() {
		super();
		this.walkerColor =  walkerColor;
		this.ShadowColor = ShadowColor;
		
	}
	
	@Override
	public void moveRight() {
		this.walker_x = this.walker_x + this.stepSize;
	}
	@Override
	public void moveLeft() {
		this.walker_x = this.walker_x - this.stepSize;
		
	}
	@Override
	public void moveUp() {
		this.walker_y = this.walker_y - this.stepSize;
	}
	@Override
	public void moveDown() {
		this.walker_y = this.walker_y + this.stepSize;
	}
	
	
	public Color getWalkerColor() {
		return walkerColor;
	}

	public void setWalkerColor(Color walkerColor) {
		this.walkerColor = walkerColor;
	}

	public Color getShadowColor() {
		return ShadowColor;
	}

	public void setWalkerShadow(Color walkerShadow) {
		this.ShadowColor = walkerShadow;
	}

	
	
}
