
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

/**
 * Assignment for Lab 9:
 * Friday 9-Mar-2018
 * 
 * This project contains both what we practiced and what
 * is to be completed as your homework.
 * 
 * 
 * @author Azim Ahmadzadeh - https://grid.cs.gsu.edu/~aahmadzadeh1/
 *
 */


/**
 * This project is implemented to let the students to practice
 * more on the topics of inheritance, encapsulation, abstraction
 * and interfaces.
 */
public class Main {
	

	public static void main(String arg[]){

		JFrame frame = new JFrame("Use Arrows to Draw, Space to Erase");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800,600);



		/* ******************************************************************* */
		/*
		 * TASK 1: Create a SimpleWalker and pass it to Ground's constructor.
		 */

		//TODO
		Walker w1 = new SimpleWalker();
		((SimpleWalker)w1).setWalkerColor(Color.WHITE);
		((SimpleWalker)w1).setWalkerShadow(Color.MAGENTA);
		// ((SimpleWalker)w1).setBounds();

		Ground panel1 = new Ground(w1);
		frame.setContentPane(panel1);
		frame.setVisible(true);
		panel1.setFocusable(true);

		/*
		 * TASK 2: Create an AdvancedWalker, set colors to it, change its stepSize, and 
		 * also make it larger than the SimpleWalker. Then pass it to the Ground's
		 * constructor.
		 */
		//TODO
		Walker w2 = new AdvancedWalker();
		((AdvancedWalker)w2).setColorToWalker(Color.YELLOW);;
		((AdvancedWalker)w2).setColorToShadow(Color.BLUE);;
		((AdvancedWalker)w2).setStepSiaze(20);
		((AdvancedWalker)w2).enlarge(20);
		//  ((AdvancedWalker)w2).setBounds();





		Ground panel = new Ground(w2); 
		panel.setFocusable(true);
		frame.setContentPane(panel);
		panel.setBackground(Color.WHITE);

		frame.setVisible(true); 




		/* ******************************************************************* */



	}





}
