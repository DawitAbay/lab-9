import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import java.awt.*;

public abstract class Walker {

	protected int initial_x =100;
	protected int initial_y =100;
	protected int walker_x = initial_x;
	protected int walker_y = initial_y;
	protected int wWidth = 5;
	protected int wHeigh = 5;
	protected int stepSize = 8;
	
//	protected int width  ;
//	protected int height ;
//	protected int x  ;
//	protected int y ;
	
	
	public abstract void moveRight ();
	public abstract void moveLeft ();
	public abstract void moveDown();
	public abstract void moveUp();
	 
//    public void setBounds(int width ,int height) 
//    {
//        // Set the new x and y coordinates, depending on which direction we have moved
//    	walker_x += initial_y;
//    	walker_y += initial_x;
//
//        if (walker_x == 0) {
//        	walker_x = 0;
//        } else if (walker_x + stepSize > wWidth) {
//        	walker_x = wWidth - stepSize;
//        }
//        if (walker_y == 0) {
//        	walker_y = 0;
//        } else if (walker_y + stepSize > wHeigh) {
//        	walker_y = wHeigh - stepSize;
//        }
//
//      
//    }
	
	public int getInitial_x() {
		return initial_x;
	}
	
	public void setInitial_x(int initial_x) {
		this.initial_x = initial_x;
	}
	
	public int getInitial_y() {
		return initial_y;
	}
	
	public void setInitial_y(int initial_y) {
		this.initial_y = initial_y;
	}
	
	public int getWalker_x() {
		return walker_x ;
	}
	
	public int getWalker_y() {
		return walker_y;
	}
	
	public void setWalker_x(int walker_x) {
		this.walker_x = walker_x;
	}
	public void setWalker_y(int walker_x) {
		this.walker_y = walker_y;
	}
	
	public int getwWidth() {
		return wWidth;
	}
	
	public void setwWidth(int wWidth) {
		this.wWidth = wWidth;
	}
	
	public int getwHeigh() {
		return wHeigh ;
	}
	public void setwHeigh(int wHeigh) {
		this.wHeigh = wHeigh;
	}

	

}
