import java.awt.Color;

public interface ICustomizable {
	public void setColorToWalker(Color Dawit) ;
	public void setShadowToWalker(Color Tony) ;
	public void setStepSiaze(int s) ;
	public void enlarge(int e) ;
}
